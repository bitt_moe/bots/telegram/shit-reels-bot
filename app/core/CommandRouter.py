from aiogram import Dispatcher, types

from core.InlineHandler import InlineHandler
from core.Command import Command
from core.Commands import get_commands


class CommandRouter:

    def __init__(self, dispatcher: Dispatcher, commands_root_folder: str):
        self.__dispatcher: Dispatcher = dispatcher
        self.__registered_commands: list[id(Command)] = []
        self.__commands_names: list[types.BotCommand] = []
        self.__word_trigger_list: dict[str, id(Command)] = {}
        self.inline_handler = InlineHandler(self.__registered_commands).handler
        self.commands: list[id(Command)] = get_commands(commands_root_folder)

    def init(self):
        for command in self.commands:
            inited_command = command(self.__dispatcher)
            self.__dispatcher.register_message_handler(
                callback=inited_command.handler,
                commands=inited_command.name
            )
            self.__handle_words_trigger(inited_command)
            self.__registered_commands.append(inited_command)
            self.__commands_names.append(types.BotCommand(inited_command.name, inited_command.description))
        self.__register_inline_commands()
        self.__register_chat_messages_listener()

    async def __chat_messages_listener(self, message: types.Message):
        for word in self.__word_trigger_list.keys():
            if message.text.lower().find(word) != -1:
                command = self.__word_trigger_list[word]
                await command.chat_message_handler(message=message)

    async def register_commands_names(self):
        """
        Регистрация команд бота так, чтобы их можно было увидеть в контекстном меню
        """
        await self.__dispatcher.bot.set_my_commands(self.__commands_names)

    def __register_inline_commands(self):
        """
        Регистрация инлайн команд
        """
        self.__dispatcher.register_inline_handler(callback=self.inline_handler)

    def __register_chat_messages_listener(self):
        self.__dispatcher.register_message_handler(callback=self.__chat_messages_listener)

    def __handle_words_trigger(self, command: id(Command)):
        words = command.chat_trigger_list
        for word in words:
            self.__word_trigger_list.update({
                word: command
            })
