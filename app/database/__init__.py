from peewee import *

from config.DatabaseConfig import DatabaseConfig


class DataBase:
    instance = SqliteDatabase(DatabaseConfig().filename)


class BaseModel(Model):
    class Meta:
        database = DataBase.instance
